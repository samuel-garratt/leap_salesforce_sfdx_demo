Feature: Property

A property stores information about the location, size, and price of a building.
It is what broker's sell to make money.

    Scenario: Create a property without a state
        Given I am a 'Property manager'
        And I am creating a Property
#        And I do not set the state
        And I set the state to 'NZ'
        When I save
        Then it is created successfully
