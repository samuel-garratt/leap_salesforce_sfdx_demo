Given('I am a member of the {string}') do |username|
  LeapSalesforce.api_user = LeapSalesforce::Users.where description: username
end

And('I am creating a {word}') do |object_name|
  @object = FactoryBot.build(object_name.snakecase.to_sym)
end

And('I set the {string} to {string}') do |field_name, value|
  @object.send("#{field_name.to_key_name}=", value)
end

When('I save') do
  @object.save!
  raise @object.diagnose_error if @object.error_message?
end

Then('the {string} is {string}') do |field_name, value|
  expect(@object.send(field_name.to_key_name)).to eq value
end
