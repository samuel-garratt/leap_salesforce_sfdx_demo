# frozen_string_literal: true

# Add users to LeapSalesforce context. First user is the default
module LeapSalesforce
  Users.add User.new :admin, 'samuel.garratt@brave-otter-ttxype.com'
  Users.add User.new :cet, 'test.cet@brave-otter-ttxype.com',
                     description: 'Enquiries team'
end

if LeapSalesforce.environment == 'uat'
  ENV['SF_USERNAME'] = ENV['UAT_USERNAME']
  ENV['SF_CONSUMER_KEY'] = ENV['UAT_CONSUMER_KEY']
end
