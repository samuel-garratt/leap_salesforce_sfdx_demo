# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :account do
    account_name { Faker::Lorem.paragraph_by_chars(number: 255) }
    trait :all do
      account_id { 'Best to not hard code this' }
      deleted { true }
      # Please add MasterRecord to .leap_salesforce.yml (if it's a table) to create association for MasterRecord
      account_type { Account::AccountType.sample }
      # Please add Parent to .leap_salesforce.yml (if it's a table) to create association for Parent
      billing_street { 'Content depending on textarea' }
      billing_city { Faker::Lorem.paragraph_by_chars(number: 40) }
      billing_state_province { Faker::Lorem.paragraph_by_chars(number: 80) }
      billing_zip_postal_code { Faker::Lorem.paragraph_by_chars(number: 20) }
      billing_country { Faker::Lorem.paragraph_by_chars(number: 80) }
      billing_latitude { 'Content depending on double' }
      billing_longitude { 'Content depending on double' }
      billing_geocode_accuracy { Account::BillingGeocodeAccuracy.sample }
      billing_address { 'Content depending on address' }
      shipping_street { 'Content depending on textarea' }
      shipping_city { Faker::Lorem.paragraph_by_chars(number: 40) }
      shipping_state_province { Faker::Lorem.paragraph_by_chars(number: 80) }
      shipping_zip_postal_code { Faker::Lorem.paragraph_by_chars(number: 20) }
      shipping_country { Faker::Lorem.paragraph_by_chars(number: 80) }
      shipping_latitude { 'Content depending on double' }
      shipping_longitude { 'Content depending on double' }
      shipping_geocode_accuracy { Account::ShippingGeocodeAccuracy.sample }
      shipping_address { 'Content depending on address' }
      account_phone { 'Content depending on phone' }
      account_fax { 'Content depending on phone' }
      account_number { Faker::Lorem.paragraph_by_chars(number: 40) }
      website { 'Content depending on url' }
      photo_url { 'Content depending on url' }
      sic_code { Faker::Lorem.paragraph_by_chars(number: 20) }
      industry { Account::Industry.sample }
      annual_revenue { 'Content depending on currency' }
      employees { 'Content depending on int' }
      ownership { Account::Ownership.sample }
      ticker_symbol { Faker::Lorem.paragraph_by_chars(number: 20) }
      account_description { 'Content depending on textarea' }
      account_rating { Account::AccountRating.sample }
      account_site { Faker::Lorem.paragraph_by_chars(number: 80) }
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
      last_activity { 'Content depending on date' }
      last_viewed_date { 'Content depending on datetime' }
      last_referenced_date { 'Content depending on datetime' }
      datacom_key { Faker::Lorem.paragraph_by_chars(number: 20) }
      jigsaw_company_id { Faker::Lorem.paragraph_by_chars(number: 20) }
      clean_status { Account::CleanStatus.sample }
      account_source { Account::AccountSource.sample }
      duns_number { Faker::Lorem.paragraph_by_chars(number: 9) }
      tradestyle { Faker::Lorem.paragraph_by_chars(number: 255) }
      naics_code { Faker::Lorem.paragraph_by_chars(number: 8) }
      naics_description { Faker::Lorem.paragraph_by_chars(number: 120) }
      year_started { Faker::Lorem.paragraph_by_chars(number: 4) }
      sic_description { Faker::Lorem.paragraph_by_chars(number: 80) }
      # Please add DandbCompany to .leap_salesforce.yml (if it's a table) to create association for DandbCompany
      customer_priority { Account::CustomerPriority.sample }
      sla { Account::SLA.sample }
      active { Account::Active.sample }
      number_of_locations { 'Content depending on double' }
      upsell_opportunity { Account::UpsellOpportunity.sample }
      sla_serial_number { Faker::Lorem.paragraph_by_chars(number: 10) }
      sla_expiration_date { 'Content depending on date' }
    end
  end
end
