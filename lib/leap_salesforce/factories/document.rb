# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :document do
    trait :all do
      content_document_id { 'Best to not hard code this' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      created { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      last_modified_date { 'Content depending on datetime' }
      is_archived { true }
      # Please add  to .leap_salesforce.yml (if it's a table) to create association for
      archived_date { 'Content depending on date' }
      is_deleted { true }
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      system_modstamp { 'Content depending on datetime' }
      title { Faker::Lorem.paragraph_by_chars(number: 255) }
      publish_status { Document::PublishStatus.sample }
      # Please add LatestPublishedVersion to .leap_salesforce.yml (if it's a table) to create association for LatestPublishedVersion
      # Please add  to .leap_salesforce.yml (if it's a table) to create association for
      last_viewed_date { 'Content depending on datetime' }
      last_referenced_date { 'Content depending on datetime' }
      description { 'Content depending on textarea' }
      size { 'Content depending on int' }
      file_type { Faker::Lorem.paragraph_by_chars(number: 20) }
      file_extension { Faker::Lorem.paragraph_by_chars(number: 40) }
      prevent_others_from_sharing_and_unsharing { Document::Preventothersfromsharingandunsharing.sample }
      file_privacy_on_records { Document::FilePrivacyonRecords.sample }
      content_modified_date { 'Content depending on datetime' }
      # Please add ContentAsset to .leap_salesforce.yml (if it's a table) to create association for ContentAsset
    end
  end
end
