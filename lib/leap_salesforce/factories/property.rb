# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :property do
    location_latitude { '-71.110448' }
    location_longitude { '42.360642' }
    State__c { 'Default State' }
    property_name { Faker::Lorem.paragraph_by_chars(number: 40) }

    trait :no_state do
      unset { :State__c }
    end

    trait :all do
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      deleted { true }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
      last_activity_date { 'Content depending on date' }
      last_viewed_date { 'Content depending on datetime' }
      last_referenced_date { 'Content depending on datetime' }
      address { Faker::Lorem.paragraph_by_chars(number: 100) }
      asking_price { 'Content depending on double' }
      assessed_value { 'Content depending on currency' }
      baths { 'Content depending on double' }
      beds { 'Content depending on double' }
      # Please add Broker__r to .leap_salesforce.yml (if it's a table) to create association for Broker__r
      city { Faker::Lorem.paragraph_by_chars(number: 50) }
      date_agreement { 'Content depending on date' }
      date_closed { 'Content depending on date' }
      date_contracted { 'Content depending on date' }
      date_listed { 'Content depending on date' }
      date_pre_market { 'Content depending on date' }
      days_on_market { 'Content depending on double' }
      description { 'Content depending on textarea' }
      location { 'Content depending on location' }
      main_picture { Faker::Lorem.paragraph_by_chars(number: 1300) }
      picture { 'Content depending on url' }
      predicted_days_on_market { 'Content depending on double' }
      price_sold { 'Content depending on currency' }
      price { 'Content depending on currency' }
      record_link { Faker::Lorem.paragraph_by_chars(number: 1300) }

      status { Property::Status.sample }
      tags { Faker::Lorem.paragraph_by_chars(number: 255) }
      main_thumbnail { Faker::Lorem.paragraph_by_chars(number: 1300) }
      thumbnail { 'Content depending on url' }
      title { Faker::Lorem.paragraph_by_chars(number: 100) }
      zip { Faker::Lorem.paragraph_by_chars(number: 10) }
    end
  end
end
