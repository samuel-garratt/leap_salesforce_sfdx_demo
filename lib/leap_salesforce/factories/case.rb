# frozen_string_literal: true

# Basic factory settings made according to values defined at
# https://developer.salesforce.com/docs/atlas.en-us.api.meta/api/sforce_api_objects_case.htm
FactoryBot.define do
  factory :case do
    subject { Faker::Lorem.paragraph_by_chars(number: 255) }
    case_origin { Case::CaseOrigin.sample }
    trait :all do
      case_id { 'Best to not hard code this' }
      deleted { true }
      case_number { Faker::Lorem.paragraph_by_chars(number: 30) }
      association :contact_id, factory: :contact
      association :account_id, factory: :account
      # Please add Asset to .leap_salesforce.yml (if it's a table) to create association for Asset
      # Please add Parent to .leap_salesforce.yml (if it's a table) to create association for Parent
      name { Faker::Lorem.paragraph_by_chars(number: 80) }
      email_address { 'Content depending on email' }
      phone { Faker::Lorem.paragraph_by_chars(number: 40) }
      company { Faker::Lorem.paragraph_by_chars(number: 80) }
      case_type { Case::CaseType.sample }
      status { Case::Status.sample }
      case_reason { Case::CaseReason.sample }
      priority { Case::Priority.sample }
      description { 'Content depending on textarea' }
      closed { true }
      closed_date { 'Content depending on datetime' }
      escalated { true }
      owner_id { User.find(CreatedDate: "<#{0.days.ago}").id }
      created_date { 'Content depending on datetime' }
      # Please add CreatedBy to .leap_salesforce.yml (if it's a table) to create association for CreatedBy
      last_modified_date { 'Content depending on datetime' }
      # Please add LastModifiedBy to .leap_salesforce.yml (if it's a table) to create association for LastModifiedBy
      system_modstamp { 'Content depending on datetime' }
      contact_phone { 'Content depending on phone' }
      contact_mobile { 'Content depending on phone' }
      contact_email { 'Content depending on email' }
      contact_fax { 'Content depending on phone' }
      internal_comments { 'Content depending on textarea' }
      last_viewed_date { 'Content depending on datetime' }
      last_referenced_date { 'Content depending on datetime' }
      engineering_req_number { Faker::Lorem.paragraph_by_chars(number: 12) }
      sla_violation { Case::SLAViolation.sample }
      product { Case::Product.sample }
      potential_liability { Case::PotentialLiability.sample }
    end
  end
end
