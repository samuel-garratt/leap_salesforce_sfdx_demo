# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Account, Industry
class Account < SoqlData
  # Enumeration for Industry
  module Industry
    include SoqlEnum

    @agriculture = 'Agriculture'

    @apparel = 'Apparel'

    @banking = 'Banking'

    @biotechnology = 'Biotechnology'

    @chemicals = 'Chemicals'

    @communications = 'Communications'

    @construction = 'Construction'

    @consulting = 'Consulting'

    @education = 'Education'

    @electronics = 'Electronics'

    @energy = 'Energy'

    @engineering = 'Engineering'

    @entertainment = 'Entertainment'

    @environmental = 'Environmental'

    @finance = 'Finance'

    @food_beverage = 'Food & Beverage'

    @government = 'Government'

    @healthcare = 'Healthcare'

    @hospitality = 'Hospitality'

    @insurance = 'Insurance'

    @machinery = 'Machinery'

    @manufacturing = 'Manufacturing'

    @media = 'Media'

    @not_for_profit = 'Not For Profit'

    @recreation = 'Recreation'

    @retail = 'Retail'

    @shipping = 'Shipping'

    @technology = 'Technology'

    @telecommunications = 'Telecommunications'

    @transportation = 'Transportation'

    @utilities = 'Utilities'

    @other = 'Other'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Industry'
      end

      # @return [Array] List of values for Industry
      def values
        ['Agriculture', 'Apparel', 'Banking', 'Biotechnology', 'Chemicals', 'Communications', 'Construction', 'Consulting', 'Education', 'Electronics', 'Energy', 'Engineering', 'Entertainment', 'Environmental', 'Finance', 'Food & Beverage', 'Government', 'Healthcare', 'Hospitality', 'Insurance', 'Machinery', 'Manufacturing', 'Media', 'Not For Profit', 'Recreation', 'Retail', 'Shipping', 'Technology', 'Telecommunications', 'Transportation', 'Utilities', 'Other']
      end

      attr_reader :agriculture, :apparel, :banking, :biotechnology, :chemicals, :communications, :construction, :consulting, :education, :electronics, :energy, :engineering, :entertainment, :environmental, :finance, :food_beverage, :government, :healthcare, :hospitality, :insurance, :machinery, :manufacturing, :media, :not_for_profit, :recreation, :retail, :shipping, :technology, :telecommunications, :transportation, :utilities, :other
    end
  end
end
