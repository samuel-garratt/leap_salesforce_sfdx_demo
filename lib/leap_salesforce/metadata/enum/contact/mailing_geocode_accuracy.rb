# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Contact, MailingGeocodeAccuracy
class Contact < SoqlData
  # Enumeration for MailingGeocodeAccuracy
  module MailingGeocodeAccuracy
    include SoqlEnum

    @address = 'Address'

    @near_address = 'NearAddress'

    @block = 'Block'

    @street = 'Street'

    @extended_zip = 'ExtendedZip'

    @zip = 'Zip'

    @neighborhood = 'Neighborhood'

    @city = 'City'

    @county = 'County'

    @state = 'State'

    @unknown = 'Unknown'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Mailing Geocode Accuracy'
      end

      # @return [Array] List of values for MailingGeocodeAccuracy
      def values
        %w[Address NearAddress Block Street ExtendedZip Zip Neighborhood City County State Unknown]
      end

      attr_reader :address, :near_address, :block, :street, :extended_zip, :zip, :neighborhood, :city, :county, :state, :unknown
    end
  end
end
