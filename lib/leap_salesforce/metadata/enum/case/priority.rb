# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Helps handle picklist values for Case, Priority
class Case < SoqlData
  # Enumeration for Priority
  module Priority
    include SoqlEnum

    @high = 'High'

    @medium = 'Medium'

    @low = 'Low'

    class << self
      # @return [String] Sample value from Enum
      def sample
        values.sample
      end

      # @return [String] Name of picklist as returned from Metadata
      def name
        'Priority'
      end

      # @return [Array] List of values for Priority
      def values
        %w[High Medium Low]
      end

      attr_reader :high, :medium, :low
    end
  end
end
