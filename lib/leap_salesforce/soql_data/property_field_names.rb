# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Property mapped from SOQL Property__c
class Property < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'Record ID', type 'id'
    soql_element :record_id, 'Id'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'Deleted', type 'boolean'
    soql_element :deleted, 'IsDeleted'

    # Element for 'Property Name', type 'string'
    soql_element :property_name, 'Name'

    # Element for 'Created Date', type 'datetime'
    soql_element :created_date, 'CreatedDate'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Last Activity Date', type 'date'
    soql_element :last_activity_date, 'LastActivityDate'

    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'

    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'

    # Element for 'Address', type 'string'
    soql_element :address, 'Address__c'

    # Element for 'Asking Price', type 'double'
    soql_element :asking_price, 'Asking_Price__c'

    # Element for 'Assessed Value', type 'currency'
    soql_element :assessed_value, 'Assessed_Value__c'

    # Element for 'Baths', type 'double'
    soql_element :baths, 'Baths__c'

    # Element for 'Beds', type 'double'
    soql_element :beds, 'Beds__c'

    # Element for 'Broker', type 'reference'
    soql_element :broker, 'Broker__c'

    # Element for 'City', type 'string'
    soql_element :city, 'City__c'

    # Element for 'Date Agreement', type 'date'
    soql_element :date_agreement, 'Date_Agreement__c'

    # Element for 'Date Closed', type 'date'
    soql_element :date_closed, 'Date_Closed__c'

    # Element for 'Date Contracted', type 'date'
    soql_element :date_contracted, 'Date_Contracted__c'

    # Element for 'Date Listed', type 'date'
    soql_element :date_listed, 'Date_Listed__c'

    # Element for 'Date Pre Market', type 'date'
    soql_element :date_pre_market, 'Date_Pre_Market__c'

    # Element for 'Days On Market', type 'double'
    soql_element :days_on_market, 'Days_On_Market__c'

    # Element for 'Description', type 'textarea'
    soql_element :description, 'Description__c'

    # Element for 'Location (Latitude)', type 'double'
    soql_element :location_latitude, 'Location__Latitude__s'

    # Element for 'Location (Longitude)', type 'double'
    soql_element :location_longitude, 'Location__Longitude__s'

    # Element for 'Location', type 'location'
    soql_element :location, 'Location__c'

    # Element for 'Main Picture', type 'string'
    soql_element :main_picture, 'Picture_IMG__c'

    # Element for 'Picture', type 'url'
    soql_element :picture, 'Picture__c'

    # Element for 'Predicted Days on Market', type 'double'
    soql_element :predicted_days_on_market, 'Predicted_Days_on_Market__c'

    # Element for 'Price Sold', type 'currency'
    soql_element :price_sold, 'Price_Sold__c'

    # Element for 'Price', type 'currency'
    soql_element :price, 'Price__c'

    # Element for 'Record Link', type 'string'
    soql_element :record_link, 'Record_Link__c'

    # Element for 'State', type 'string'
    soql_element :state, 'State__c'

    # Element for 'Status', type 'picklist'
    soql_element :status, 'Status__c'

    # Element for 'Tags', type 'string'
    soql_element :tags, 'Tags__c'

    # Element for 'Main Thumbnail', type 'string'
    soql_element :main_thumbnail, 'Thumbnail_IMG__c'

    # Element for 'Thumbnail', type 'url'
    soql_element :thumbnail, 'Thumbnail__c'

    # Element for 'Title', type 'string'
    soql_element :title, 'Title__c'

    # Element for 'Zip', type 'string'
    soql_element :zip, 'Zip__c'
  end
end
