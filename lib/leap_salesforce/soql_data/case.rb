# frozen_string_literal: true

require_relative 'case_field_names'
# An Case object mapping to a SOQL Case
class Case < SoqlData
  include Case::Fields
end
