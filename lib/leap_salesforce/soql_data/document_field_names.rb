# frozen_string_literal: true

# This file is generated and updated automatically so best not to edit manually
# Fields for Document mapped from SOQL ContentDocument
class Document < SoqlData
  module Fields
    extend SoqlGlobalObjectData

    # Element for 'ContentDocument ID', type 'id'
    soql_element :content_document_id, 'Id'

    # Element for 'Created By ID', type 'reference'
    soql_element :created_by_id, 'CreatedById'

    # Element for 'Created', type 'datetime'
    soql_element :created, 'CreatedDate'

    # Element for 'Last Modified By ID', type 'reference'
    soql_element :last_modified_by_id, 'LastModifiedById'

    # Element for 'Last Modified Date', type 'datetime'
    soql_element :last_modified_date, 'LastModifiedDate'

    # Element for 'Is Archived', type 'boolean'
    soql_element :is_archived, 'IsArchived'

    # Element for 'User ID', type 'reference'
    soql_element :user_id, 'ArchivedById'

    # Element for 'Archived Date', type 'date'
    soql_element :archived_date, 'ArchivedDate'

    # Element for 'Is Deleted', type 'boolean'
    soql_element :is_deleted, 'IsDeleted'

    # Element for 'Owner ID', type 'reference'
    soql_element :owner_id, 'OwnerId'

    # Element for 'System Modstamp', type 'datetime'
    soql_element :system_modstamp, 'SystemModstamp'

    # Element for 'Title', type 'string'
    soql_element :title, 'Title'

    # Element for 'Publish Status', type 'picklist'
    soql_element :publish_status, 'PublishStatus'

    # Element for 'Latest Published Version ID', type 'reference'
    soql_element :latest_published_version_id, 'LatestPublishedVersionId'

    # Element for 'Parent ID', type 'reference'
    soql_element :parent_id, 'ParentId'

    # Element for 'Last Viewed Date', type 'datetime'
    soql_element :last_viewed_date, 'LastViewedDate'

    # Element for 'Last Referenced Date', type 'datetime'
    soql_element :last_referenced_date, 'LastReferencedDate'

    # Element for 'Description', type 'textarea'
    soql_element :description, 'Description'

    # Element for 'Size', type 'int'
    soql_element :size, 'ContentSize'

    # Element for 'File Type', type 'string'
    soql_element :file_type, 'FileType'

    # Element for 'File Extension', type 'string'
    soql_element :file_extension, 'FileExtension'

    # Element for 'Prevent others from sharing and unsharing', type 'picklist'
    soql_element :prevent_others_from_sharing_and_unsharing, 'SharingOption'

    # Element for 'File Privacy on Records', type 'picklist'
    soql_element :file_privacy_on_records, 'SharingPrivacy'

    # Element for 'Content Modified Date', type 'datetime'
    soql_element :content_modified_date, 'ContentModifiedDate'

    # Element for 'Asset File ID', type 'reference'
    soql_element :asset_file_id, 'ContentAssetId'
  end
end
