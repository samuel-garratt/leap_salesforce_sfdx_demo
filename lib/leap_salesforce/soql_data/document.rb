# frozen_string_literal: true

require_relative 'document_field_names'
# An Document object mapping to a SOQL ContentDocument
class Document < SoqlData
  include Document::Fields
  soql_object 'ContentDocument'
end
