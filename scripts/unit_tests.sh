sfdx force:apex:test:run --targetusername $SCRATCH_ORG_ALIAS --wait 10 --resultformat human --codecoverage --testlevel $TESTLEVEL
# TODO: Figure out how to have both junit and human readable formats in one command
mkdir logs
sfdx force:apex:test:run --targetusername $SCRATCH_ORG_ALIAS --wait 10 -r junit --testlevel $TESTLEVEL >> logs/apex.xml
